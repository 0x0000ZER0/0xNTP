#include <stdint.h>
#include <string.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <stdio.h>

typedef struct {
	uint8_t  li_vn_mode;
	uint8_t  startum;
	uint8_t  poll;
	uint8_t  precision;
	uint32_t delay;
	uint32_t error;
	uint32_t ref_id;
	uint64_t ref_time;
	uint64_t orig_time;
	uint64_t recv_time;
	uint32_t ts_time_s;
	uint32_t ts_time_f;
} ntp_packet;

typedef struct hostent	   hostent;
typedef struct sockaddr	   sockaddr;
typedef struct sockaddr_in sockaddr_in;

#define UNIX_TIME_DELTA 2208988800ULL // [01.01.1970] in seconds.

int
main() {
	int sock;
	sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);		

	sockaddr_in server;
	memset(&server, 0, sizeof (server)); 

	server.sin_family 	= AF_INET;
	server.sin_port   	= htons(123); // default PORT for NTP.

	hostent *host;
	host = gethostbyname("us.pool.ntp.org");
	
	memcpy((void*)&server.sin_addr.s_addr, (void*)host->h_addr, host->h_length);
	if (server.sin_addr.s_addr == INADDR_NONE) {
		fprintf(stderr, "ERROR: could not find the host.\n");
		close(sock);
		return -1;
	}
	
	ntp_packet packet;
	memset(&packet, 0, sizeof (packet));

	packet.li_vn_mode = 0x1B; // HEX representation of 0b00011011. 
				  // Used for setting the version of the protocol.

	ssize_t bytes;

	bytes = sendto(sock, &packet, sizeof (packet), 0, (sockaddr*)&server, sizeof (server));
	if (bytes == -1) {
		perror("ERROR: unable to send the request. Please try again");
		close(sock);
		return -1;
	}

	bytes = recvfrom(sock, &packet, sizeof (packet), 0, NULL, NULL);
	if (bytes == -1) {
		perror("ERROR: unable to receive the data. Please try again");
		close(sock);
		return -1;
	}

	uint32_t secs;
	secs = htonl(packet.ts_time_s);

	time_t time;
	time = (time_t)(secs - UNIX_TIME_DELTA);

	printf("INFO: %s\n", ctime(&time));	

	close(sock);

	return 0;
}
